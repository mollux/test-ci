vcl 4.0;
import std;
import directors;

# Default backend definition. Points to Apache, normally.
backend default {
    .host = "BACKEND_HOST";
    .port = "BACKEND_PORT";
    .connect_timeout = 10s;
    .first_byte_timeout = 60s;
    .between_bytes_timeout = 60s;
}

# Access control list for PURGE requests.
acl purge {
    "BACKEND_HOST";
}

# Respond to incoming requests.
sub vcl_recv {
    # Protecting against the HTTPOXY CGI vulnerability.
    unset req.http.proxy;
    # Add an X-Forwarded-For header with the client IP address.
    if (req.restarts == 0) {
        if (req.http.X-Forwarded-For) {
            set req.http.X-Forwarded-For = req.http.X-Forwarded-For + ", " + client.ip;
        }
        else {
            set req.http.X-Forwarded-For = client.ip;
        }
    }

    # Store original url in temporary header
    set req.http.Original-Url = req.url;

    # Only allow PURGE requests from IP addresses in the 'purge' ACL.
    if (req.method == "PURGE") {
        if (!client.ip ~ purge) {
            return (synth(405, "Not allowed."));
        }
        return (hash);
    }

    # Only allow BAN requests from IP addresses in the 'purge' ACL.
    if (req.method == "BAN") {
        # Same ACL check as above:
        if (!client.ip ~ purge) {
            return (synth(403, "Not allowed."));
        }

        # Logic for banning based on tags
        # https://varnish-cache.org/docs/trunk/reference/vcl.html#vcl-7-ban
        if (req.http.X-Dropsolid-Purge-Tags) {
            # Add bans for tags but only for the current site requesting the ban
            ban("obj.http.X-Dropsolid-Purge-Tags ~ " + req.http.X-Dropsolid-Purge-Tags + " && obj.http.X-Dropsolid-Site == " + req.http.X-Dropsolid-Purge);
            return (synth(200, "Ban added."));
        }

        # Logic for banning everything
        if (req.http.X-Dropsolid-Purge-All) {
            # Add bans for the whole site
            ban("obj.http.X-Dropsolid-Site == " + req.http.X-Dropsolid-Purge);
            return (synth(200, "Ban added."));
        }

        # Throw a synthetic page so the request won't go to the backend.
        return (synth(403, "Missing headers for a ban"));
    }

    # Only cache GET and HEAD requests (pass through POST requests).
    if (req.method != "GET" && req.method != "HEAD") {
        return (pass);
    }

    # Pass through any administrative or AJAX-related paths.
    if (req.url ~ "^/status\.php$" ||
        req.url ~ "^/update\.php$" ||
        req.url ~ "^/admin$" ||
        req.url ~ "^/admin/.*$" ||
        req.url ~ "^.*/system/files/.*$" ||
        req.url ~ "^/flag/.*$" ||
        req.url ~ "^.*/ajax/.*$" ||
        req.url ~ "^.*/ahah/.*$" ||
        req.url ~ "^/simplesaml" ||
        req.url ~ "^/simplesaml/.*$" ||
        req.url ~ "^/saml_login") {
            return (pass);
    }

    # Implementing websocket support (https://www.varnish-cache.org/docs/4.0/users-guide/vcl-example-websockets.html).
    if (req.http.Upgrade ~ "(?i)websocket") {
        return (pipe);
    }

    # Some generic URL manipulation, useful for all templates that follow
    # First remove URL parameters used to track effectiveness of online marketing campaigns
    if (req.url ~ "(\?|&)(utm_[a-z]+|gclid|cx|ie|cof|siteurl|fbclid|__hs[a-z]+)=") {
        set req.url = regsuball(req.url, "(utm_[a-z]+|gclid|cx|ie|cof|siteurl|fbclid|__hs[a-z]+)=[-_A-z0-9+()%.]+&?", "");
        set req.url = regsub(req.url, "[?|&]+$", "");
    }

    # Strip hash, server doesn't need it.
    if (req.url ~ "\#") {
        set req.url = regsub(req.url, "\#.*$", "");
    }

    # Strip a trailing ? if it exists.
    if (req.url ~ "\?$") {
        set req.url = regsub(req.url, "\?$", "");
    }

    # Pass all files that are almost certain to be static (or once generated static) to the backend.
    # We keep the cookie for those files that aren't actual files (e.g. a download link to a generated xls export)
    # The cookie won't give any overhead (except the header size) to the backend, and is needed for e.g. authenticated files.
    if (req.url ~ "(?i)\.(pdf|asc|dat|txt|doc|docx|xls|xlsx|ppt|pptx|tgz|csv|png|gif|jpeg|jpg|ico|swf|css|js|svg|ttf|eot|otf|woff|woff2)(\?.*)?$") {
      return (pass);
    }

    # Remove all cookies that Drupal doesn't need to know about. We explicitly
    # list the ones that Drupal does need, the SESS and NO_CACHE. If, after
    # running this code we find that either of these two cookies remains, we
    # will pass as the page cannot be cached.
    if (req.http.Cookie) {
        # 0. Store any vary prefixed cookie in a separate value for hashing later.
        # 1. Append a semi-colon to the front of the cookie string.
        # 2. Remove all spaces that appear after semi-colons.
        # 3. Match the cookies we want to keep, adding the space we removed
        #    previously back. (\1) is first matching group in the regsuball.
        # 4. Remove all other cookies, identifying them by the fact that they have
        #    no space after the preceding semi-colon.
        # 5. Remove all spaces and semi-colons from the beginning and end of the
        #    cookie string.

        # Set our value our cookie vary hash to see if we need
        # to make a variation for it.
        set req.http.X-Dropsolid-Cookie-Vary-Hash = ";" + req.http.Cookie;
        set req.http.X-Dropsolid-Cookie-Vary-Hash = regsuball(req.http.X-Dropsolid-Cookie-Vary-Hash, "; +", ";");

        # See if we have a Dropsolid specific vary by cookie
        set req.http.X-Dropsolid-Cookie-Vary-Hash = regsuball(req.http.X-Dropsolid-Cookie-Vary-Hash, ";(DS_VARY_[a-zA-Z0-9]+)=", "; \1=");
        set req.http.X-Dropsolid-Cookie-Vary-Hash = regsuball(req.http.X-Dropsolid-Cookie-Vary-Hash, ";[^ ][^;]*", "");
        set req.http.X-Dropsolid-Cookie-Vary-Hash = regsuball(req.http.X-Dropsolid-Cookie-Vary-Hash, "^[; ]+|[; ]+$", "");

        if (req.http.X-Dropsolid-Cookie-Vary-Hash == "") {
            # If there are no remaining vary hash cookies, remove the header
            unset req.http.X-Dropsolid-Cookie-Vary-Hash;
        }

        set req.http.Cookie = ";" + req.http.Cookie;
        set req.http.Cookie = regsuball(req.http.Cookie, "; +", ";");
        set req.http.Cookie = regsuball(req.http.Cookie, ";(SESS[a-z0-9]+|SSESS[a-z0-9]+|NO_CACHE|SimpleSAML[a-zA-Z0-9_]+)=", "; \1=");
        set req.http.Cookie = regsuball(req.http.Cookie, ";[^ ][^;]*", "");
        set req.http.Cookie = regsuball(req.http.Cookie, "^[; ]+|[; ]+$", "");

        if (req.http.Cookie == "") {
            # If there are no remaining cookies, remove the cookie header. If there
            # aren't any cookie headers, Varnish's default behavior will be to cache
            # the page.
            unset req.http.Cookie;
        }
        else {
            # If there is any cookies left (a session or NO_CACHE cookie), do not
            # cache the page. Pass it on to Apache directly.
            return (pass);
        }
    }
}

sub vcl_hash {
    # Hash cookie data
    # As requests with same URL and host can produce diferent results when issued with different cookies,
    # we need to store items hashed with the associated cookies. Note that cookies are already sanitized when we reach this point.
    if (req.http.Cookie) {
        # Include cookie in cache hash
        hash_data(req.http.Cookie);
    }

    # Custom header hashing.
    if ( req.http.X-Forwarded-Proto ) {
        hash_data(req.http.X-Forwarded-Proto);
    }

    # Continue with built-in logic.
    # We want built-in logic to be processed after ours so we don't call return.

    # Some cookies are allowed to produce a variation. But those are stored in a header,
    # as the cookie doesn't need to be passed to Drupal.
    if ( req.http.X-Dropsolid-Cookie-Vary-Hash ) {
        hash_data(req.http.X-Dropsolid-Cookie-Vary-Hash);
    }
}

sub vcl_miss {
    # if we get here and there is a Dropsolid specific vary by cookie, add it
    # back to the cookie list.
    if (req.http.X-Dropsolid-Cookie-Vary-Hash) {
        if (req.http.Cookie) {
            set req.http.Cookie = req.http.Cookie + ";" + req.http.X-Dropsolid-Cookie-Vary-Hash;
        }
        else {
            set req.http.Cookie = req.http.X-Dropsolid-Cookie-Vary-Hash;
        }
        unset req.http.X-Dropsolid-Cookie-Vary-Hash;
    }
}

sub vcl_pass {
    # if we get here and there is a Dropsolid specific vary by cookie, add it
    # back to the cookie list.
    # If the requests already passed through the vcl_miss subroutine, it won't be processed again.
    if (req.http.X-Dropsolid-Cookie-Vary-Hash) {
        if (req.http.Cookie) {
            set req.http.Cookie = req.http.Cookie + ";" + req.http.X-Dropsolid-Cookie-Vary-Hash;
        }
        else {
            set req.http.Cookie = req.http.X-Dropsolid-Cookie-Vary-Hash;
        }
        unset req.http.X-Dropsolid-Cookie-Vary-Hash;
    }
}

# Set a header to track a cache HITs and MISSes.
sub vcl_deliver {
    # Remove ban-lurker friendly custom headers when delivering to client.
    unset resp.http.X-Url;
    unset resp.http.X-Host;
    # Remove some headers
    unset resp.http.Via;
    # Comment these for easier Drupal cache tag debugging in development.
    unset resp.http.Cache-Tags;
    unset resp.http.X-Dropsolid-Purge-Tags;
    unset resp.http.X-Dropsolid-Site;
    unset resp.http.X-Drupal-Cache-Contexts;

    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
    }
    else {
        set resp.http.X-Cache = "MISS";
    }
}

# Instruct Varnish what to do in the case of certain backend responses (beresp).
sub vcl_backend_response {
    # Set ban-lurker friendly custom headers.
    set beresp.http.X-Url = bereq.url;
    set beresp.http.X-Host = bereq.http.host;

    # Don't cache items larger dan 2MB
    if (std.integer(beresp.http.content-length, 0) > 2097152) {
        set beresp.uncacheable = true;
    }

    # Cache 404s, at 500s with a short lifetime to protect the backend.
    if (beresp.status == 404 || beresp.status == 500 || beresp.status == 502 || beresp.status == 503 || beresp.status == 504) {
        set beresp.ttl = 60s;
    }

    # Do not cache errors from image styles generation in Drupal.
    # See: https://blog.42mate.com/images-styles-in-drupal-7-with-varnish/
    if (bereq.url ~ "(?i)\.(png|gif|jpeg|jpg|ico)(\?itok=.*)?$") {
        if (beresp.status == 503 || beresp.status == 500) {
            set beresp.http.cache-control = "no-cache";
            set beresp.ttl = 0s;
        }
    }

    # Enable streaming directly to backend for BigPipe responses.
    if (beresp.http.Surrogate-Control ~ "BigPipe/1.0") {
        set beresp.do_stream = true;
        set beresp.ttl = 0s;
    }

    # Allow items to remain in cache up to 6 hours past their cache expiration.
    set beresp.grace = 6h;

    # re-add the marketing query params we stripped before on a redirect.
    if (beresp.status == 301 || beresp.status == 302 || beresp.status == 307 || beresp.status == 308) {
        set bereq.http.marketing_params = regsuball(bereq.http.Original-Url, "^.*(?=\?)|[?&](?!utm_[a-z]+=|gclid=|cx=|ie=|cof=|siteurl=|fbclid=|__hs[a-z]+=)[a-zA-Z_-]+(=[^&]*)?|\#.*$", "");
        if (bereq.http.marketing_params ~ "[?&]") {
            # check if url already has query params
            if (beresp.http.location !~ "(\?)") {
                set beresp.http.location = regsub(beresp.http.location, "\#.*$", "") + "?" + bereq.http.marketing_params + regsub(beresp.http.location, "[^#]*", "");
            }
            else{
                set beresp.http.location = regsub(beresp.http.location, "\#.*$", "") + "&" + bereq.http.marketing_params + regsub(beresp.http.location, "[^#]*", "");
            }
            # strip occurrences of `&?` after removing params
            set beresp.http.location = regsub(beresp.http.location, "(&\?|&&)", "&");
            # strip occurrences of `?&` after removing params
            set beresp.http.location = regsub(beresp.http.location, "(\?&|\?\?)", "?");
            # some more cleanup (empty `?` or `&` at end)
            set beresp.http.location = regsub(beresp.http.location, "(\?&|\?|&)$", "");
        }
    }

}

sub vcl_pipe {
    # Called upon entering pipe mode.
    # In this mode, the request is passed on to the backend, and any further data from both the client
    # and backend is passed on unaltered until either end closes the connection. Basically, Varnish will
    # degrade into a simple TCP proxy, shuffling bytes back and forth. For a connection in pipe mode,
    # no other VCL subroutine will ever get called after vcl_pipe.
    # Note that only the first request to the backend will have
    # X-Forwarded-For set.  If you use X-Forwarded-For and want to
    # have it set for all requests, make sure to have:
    # set bereq.http.connection = "close";
    # here.  It is not set by default as it might break some broken web
    # applications, like IIS with NTLM authentication.
    # set bereq.http.Connection = "Close";
    # Implementing websocket support (https://www.varnish-cache.org/docs/4.0/users-guide/vcl-example-websockets.html)
    if (req.http.upgrade) {
        set bereq.http.upgrade = req.http.upgrade;
    }
    return (pipe);
}

# In the event of an error, show friendlier messages.
sub vcl_backend_error {
    set beresp.http.Content-Type = "text/html; charset=utf-8";
    synthetic ({"
<html>
<head>
    <title>Not Found</title>
    <style type="text/css">
        A:link {text-decoration: none;  color: #333333;}
        A:visited {text-decoration: none; color: #333333;}
        A:active {text-decoration: none}
        A:hover {text-decoration: underline;}
    </style>
</head>
<body onload="setTimeout(function() { window.location.reload() }, 5000)" bgcolor=white text=#333333 style="padding:5px 15px 5px 15px; font-family: myriad-pro-1,myriad-pro-2,corbel,sans-serif;">
<H3>Page Unavailable</H3>
<p>The page you requested is temporarily unavailable.</p>
</body>
</html>
"});
    return (deliver);
}