alias xdebug="xdebug --ini-file=/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"
alias addknownhost='function _addknownhost(){ ssh-keyscan $1 >> ~/.ssh/known_hosts; };_addknownhost'

function xdebug_info() {
  egrep -q -E "^\;zend_extension" /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && echo "\[\e[37;7m\]  \[\e[0m\]" || echo "\[\e[0;32;7m\]  \[\e[0m\]"
}

function useful_path() {
  echo $(pwd | rev | cut -d\/ -f-2 | rev)
}

set_bash_prompt() {
  PS1="\[\e[0;34;7m\] \$(echo $DOCKER_NETWORK | rev | cut -d- -f2- | rev) \[\e[0m\]$(xdebug_info) $(useful_path)\$ "
}

PROMPT_COMMAND=set_bash_prompt
