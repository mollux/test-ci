#! /bin/bash

# Check if group id already exists
GROUPINFO=$(getent group ${DLP_GID} | cut -d: -f1)
if [ "$GROUPINFO" == "" ]; then
	addgroup -g ${DLP_GID} dlp
else
	groupmod -n dlp ${GROUPINFO}
fi

adduser -u ${DLP_UID} -D -S -H -G dlp dlp

chown -R dlp:dlp /home/dlp
chown dlp:dlp /var/www/html

exec su-exec dlp:dlp /bin/bash
