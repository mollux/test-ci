#!/bin/bash
set -e

RED=$(echo -en '\033[00;1;31m')
GREEN=$(echo -en '\033[00;1;32m')
BLUE=$(echo -en '\033[00;1;34m')
MAGENTA=$(echo -en '\033[00;1;35m')

RESET=$(echo -en '\033[0m')

echo "     ${MAGENTA}->${RESET} test ${MAGENTA}whoami${RESET}"
docker run --entrypoint="/bin/sh" test_image:${TAG}${IMAGE_VARIANT_SUFFIX} -c "whoami" 2>&1 | sed 's/^/        /'
echo "response code: ${PIPESTATUS[0]}" 2>&1 | sed 's/^/        /'

echo "     ${MAGENTA}->${RESET} test ${MAGENTA}ls${RESET}"
docker run --entrypoint="/bin/sh" test_image:${TAG}${IMAGE_VARIANT_SUFFIX} -c "ls -al" 2>&1 | sed 's/^/        /'
echo "response code: ${PIPESTATUS[0]}" 2>&1 | sed 's/^/        /'

echo "     ${MAGENTA}->${RESET} test ${MAGENTA}version${RESET}"
docker run --entrypoint="/bin/sh" test_image:${TAG}${IMAGE_VARIANT_SUFFIX} -c "cat /etc/os-release | grep VERSION_ID" 2>&1 | sed 's/^/        /'
echo "response code: ${PIPESTATUS[0]}" 2>&1 | sed 's/^/        /'

echo "     ${MAGENTA}->${RESET} test ${MAGENTA}unexisting command${RESET}"
docker run --entrypoint="/bin/sh" test_image:${TAG}${IMAGE_VARIANT_SUFFIX} -c "unexisting" 2>&1 | sed 's/^/        /'
echo "response code: ${PIPESTATUS[0]}" 2>&1 | sed 's/^/        /'
