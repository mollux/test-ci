ARG BASE_IMAGE=php:7.1-cli-alpine

FROM $BASE_IMAGE as builder

ARG MEMCACHED_VERSION=v3.1.4
ARG REDIS_VERSION=5.1.1
ARG XDEBUG_VERSION=2.9.2
ARG XHPROF_VERSION=v5.0.2
ARG IGBINARY_VERSION=3.1.2
ARG APCU_VERSION=v5.1.18

RUN apk add --no-cache \
    autoconf \
    cmake \
    file \
    g++ \
    gcc \
    libc-dev \
    pcre-dev \
    make \
    git \
    pkgconf \
    re2c \
    icu-dev \
    libxml2-dev \
    libpng-dev \
    freetype-dev \
    libwebp-dev \
    libjpeg-turbo-dev \
    libmemcached-dev \
    libzip-dev \
    tzdata \
    oniguruma-dev

RUN docker-php-ext-configure bcmath --enable-bcmath

RUN docker-php-ext-install bcmath 

RUN mkdir /tmp/php_extensions/
RUN cp -rp $(php -r 'echo ini_get("extension_dir");')/* /tmp/php_extensions/

FROM $BASE_IMAGE
RUN apk add --no-cache mysql-client patch openssh bash git zip icu-dev libxml2 libpng freetype libwebp libjpeg-turbo rsync ncurses libmemcached-dev findutils su-exec shadow libzip-dev

ARG COMPOSER_VERSION=1.6.5

COPY --from=builder /usr/share/zoneinfo/Europe/Brussels /etc/localtime
RUN echo "Europe/Brussels" > /etc/timezone

COPY --from=builder /tmp/php_extensions /tmp/php_extensions
RUN cp -rp /tmp/php_extensions/* $(php -r 'echo ini_get("extension_dir");')/ \
    && rm -rf /tmp/php_extensions/

COPY --from=builder /usr/local/etc/php/conf.d/ /usr/local/etc/php/conf.d/

ENV PHP_MEMORY_LIMIT 256M
ENV PHP_SHORT_OPEN_TAG 0

RUN chmod 777 /usr/local/etc/php/conf.d/

RUN mkdir -p  /home/dlp/.drush/
COPY files/drushrc.php /home/dlp/.drush/drushrc.php

RUN mkdir /home/dlp/.ssh
COPY files/.bashrc /home/dlp/.bashrc

RUN mkdir -p /var/www/html

COPY files/entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

WORKDIR /var/www/html
ENTRYPOINT /entrypoint.sh
