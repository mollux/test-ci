FROM alpine:3.6

ENV VCL_CONFIG /etc/varnish/default.vcl
ENV CACHE_SIZE 64m
ENV VARNISHD_PARAMS -p default_ttl=3600 -p default_grace=3600
ENV BACKEND_HOST apache
ENV BACKEND_PORT 80

RUN apk add --no-cache varnish

COPY default.vcl /etc/varnish/default.vcl

EXPOSE 80 6082

RUN echo "launchpad_varnish_key" > /tmp/varnish_key.txt

COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
