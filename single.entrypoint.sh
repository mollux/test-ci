#!bin/sh

# We need to find and replace the varnish backend host info, as std.getenv and
# std.fileread throw a Expected CSTR got 'std.fileread' error
# see https://stackoverflow.com/a/25306572
sed -i "s|BACKEND_HOST|${BACKEND_HOST}|g" ${VCL_CONFIG}
sed -i "s|BACKEND_PORT|${BACKEND_PORT}|g" ${VCL_CONFIG}
varnishd -T 0.0.0.0:6082 -S "/tmp/varnish_key.txt" -f $VCL_CONFIG -s malloc,$CACHE_SIZE $VARNISHD_PARAMS
varnishncsa -F '%{Host}i %h %l %u %t \"%r\" %s %b \"%{Referer}i\" \"%{User-agent}i\" \"%{Varnish:hitmiss}x\"'
