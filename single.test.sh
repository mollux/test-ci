#!/bin/bash
set -e

RED=$(echo -en '\033[00;1;31m')
GREEN=$(echo -en '\033[00;1;32m')
BLUE=$(echo -en '\033[00;1;34m')
MAGENTA=$(echo -en '\033[00;1;35m')

RESET=$(echo -en '\033[0m')

echo "     ${MAGENTA}->${RESET} test ${MAGENTA}varnish is running${RESET}"
docker run --platform="$PLATFORM" -d --name varnish -p127.0.0.1:8123:80 -eBACKEND_HOST=127.0.0.1 -eBACKEND_PORT=81 test_image:${TAG}${IMAGE_VARIANT_SUFFIX} 2>&1 | sed 's/^/        /'
sleep 10
docker ps -a 2>&1 | sed 's/^/        /'
docker port varnish 2>&1 | sed 's/^/        /'
docker logs varnish 2>&1 | sed 's/^/        /'
wget -q --server-response http://127.0.0.1:8123 2>&1 | sed 's/^/        /'
docker rm -f varnish 2>&1 | sed 's/^/        /'
